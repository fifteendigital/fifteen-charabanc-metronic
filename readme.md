# Charabanc-Metronic

###Installation

Add Charabanc-Metronic to your composer.json file:

```js
    "require": {
        "php": ">=5.5.9",
        "laravel/framework": "5.2.*",
        "fifteen/charabanc": "1.1.*",
        "fifteen/charabanc-metronic": "1.0.*"
    },
```

Add minimum-stability: dev (IMPORTANT!), and as the repository isn't on Packagist, you also need to include it in the repositories list:

```sh
    "minimum-stability": "dev",
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fifteendigital/fifteen-charabanc-metronic.git"
        }
    ]
```

Run composer update:

```sh
$ composer update
```

Register service provider in config/app.php:

```php
    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        ...

        /*
         * Package Service Providers...
         */
        Fifteen\CharabancMetronic\CharabancMetronicServiceProvider::class,
    ];
```

Publish files to application:

```sh
$ php artisan vendor:publish --provider="Fifteen\CharabancMetronic\CharabancMetronicServiceProvider" --force
```

The resources/assets folder (Metronic) is zipped up - this needs to be unzipped:

```sh
$ unzip resources/assets/vendor/metronic.zip -d resources/assets/vendor && rm -f resources/assets/vendor/metronic.zip
```

Add the following to gulpfile.js, inside elixir():

```js
    // metronic
    mix
      .sass([
          // base template
          '../vendor/google-fonts/open-sans.css',
          '../vendor/font-awesome/css/font-awesome.min.css',
          '../vendor/simple-line-icons/simple-line-icons.css',  // This was edited to reference the fonts folder
          '../vendor/bootstrap/css/bootstrap.min.css',
          '../vendor/uniform/css/uniform.default.css',
          '../vendor/metronic/global/css/components.css',
          '../vendor/metronic/global/css/plugins.css',
          '../vendor/metronic/admin/layout/css/layout.css',
          '../vendor/metronic/admin/layout/css/themes/darkblue.css',  // This was edited to reference the img folder
          '../vendor/metronic/admin/layout/css/custom.css',
          // extra plugins
          '../vendor/select2/select2.css',
          '../vendor/bootstrap-colorpicker/css/colorpicker.css',
          '../vendor/datatables/extensions/Scroller/css/dataTables.scroller.min.css',
          '../vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css',
          '../vendor/datatables/plugins/bootstrap/dataTables.bootstrap.css',
          '../vendor/bootstrap-datepicker/css/datepicker.css',
          '../vendor/fancybox/source/jquery.fancybox.css',
          '../vendor/gritter/css/jquery.gritter.css',
          // '../vendor/bootstrap-modal/css/bootstrap-modal-bs3patch.css',    // Causes layout issues -- TODO: remove?
          '../vendor/bootstrap-modal/css/bootstrap-modal.css',
          '../vendor/jquery-nestable/jquery.nestable.css',
          '../vendor/uploadify/uploadify.css',
        ], './public/metronic/css/metronic.css')
      .sass([
          '../vendor/select2/select2.css',
          '../vendor/metronic/admin/pages/css/login-soft.css',
        ], './public/metronic/css/login.css')
      .sass('/../vendor/metronic/admin/pages/css/error.css', './public/metronic/css/error.css')
      .scripts([
          // base template
          '../vendor/jquery.min.js',
          '../vendor/jquery-migrate.min.js',
          '../vendor/jquery-ui/jquery-ui.min.js',
          '../vendor/bootstrap/js/bootstrap.min.js',
          '../vendor/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
          '../vendor/jquery-slimscroll/jquery.slimscroll.min.js',
          '../vendor/jquery.blockui.min.js',
          '../vendor/jquery.cokie.min.js',
          '../vendor/uniform/jquery.uniform.min.js',
          '../vendor/bootstrap-switch/js/bootstrap-switch.min.js',
          '../vendor/metronic/global/scripts/metronic.js',
          '../vendor/metronic/admin/layout/scripts/layout.js',
          '../vendor/metronic/admin/layout/scripts/quick-sidebar.js',
          // extra plugins
          '../vendor/select2/select2.min.js',
          '../vendor/datatables/media/js/jquery.dataTables.min.js',
          '../vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js',
          '../vendor/datatables/extensions/Scroller/js/dataTables.scroller.min.js',
          '../vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js',

          '../vendor/fancybox/source/jquery.fancybox.pack.js',
          '../vendor/gritter/js/jquery.gritter.js',
          '../vendor/tiny_mce/tiny_mce.js',
          // '../vendor/jquery/plugins/htmlEncode/jquery.htmlEncode.js',  // causes issue - to do with tinyMCE?
          '../vendor/jquery/plugins/imodal/imodal.js',

          '../vendor/datatables/plugins/bootstrap/dataTables.bootstrap.js',
          '../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
          '../vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
          '../vendor/bootstrap-modal/js/bootstrap-modal.js',
          '../vendor/bootstrap-modal/js/bootstrap-modalmanager.js',

          '../vendor/jquery-nestable/jquery.nestable.js',
          '../vendor/uploadify/swfobject.js',
          '../vendor/uploadify/jquery.uploadify.v2.1.4.min.js',
          'metronic-init.js',
        ], './public/metronic/js/metronic.js')
      .scripts([
          '../vendor/jquery-validation/js/jquery.validate.min.js',
          '../vendor/backstretch/jquery.backstretch.min.js',
          '../vendor/select2/select2.min.js',
          '../vendor/metronic/admin/layout/scripts/layout.js',
          '../vendor/metronic/admin/layout/scripts/demo.js',
          '../vendor/metronic/admin/pages/scripts/login-soft.js',
        ], './public/metronic/js/login.js');
```

Then add this to mix.version:
```js
          'public/metronic/css/metronic.css',
          'public/metronic/css/login.css',
          'public/metronic/css/error.css',
          'public/metronic/js/metronic.js',
          'public/metronic/js/login.js',
```
