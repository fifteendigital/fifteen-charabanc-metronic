@extends('Template::layouts.login')

@section('content')

	<!-- BEGIN LOGO -->
	<div class="logo">
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->

		{!! Form::open(['route' => 'sessions.store', 'class' => 'login-form']) !!}
			<h3 class="form-title">Login to your account</h3>
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				<span>
					{{ Alang::get('general.enter_any_email_and_password') }}.
				</span>
			</div>

			<div class="form-group">
				{!! Form::label('email', Alang::get('general.email_address'), ['class' => 'control-label visible-ie8 visible-ie9']) !!}
				<div class="input-icon">
					<i class="fa fa-user"></i>
					{!! Form::text('email', null, ['class' => 'form-control placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => 'User name']) !!}
				</div>
				{{ $errors->first('email') }}
			</div>

			<div class="form-group">
				{!! Form::label('password', Alang::get('general.password'), ['class' => 'control-label visible-ie8 visible-ie9']) !!}
				<div class="input-icon">
					<i class="fa fa-lock"></i>
					{!! Form::password('password', ['class' => 'form-control placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => 'Password']) !!}
				</div>
				{{ $errors->first('password') }}
			</div>

			<div class="form-actions">
				<label class="checkbox">
				<input type="checkbox" name="remember" value="1"/> Remember me </label>
				<button type="submit" class="btn blue pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>

			<div class="forget-password">
				<h4>Forgot your password ?</h4>
				<p>
					 no worries, click
					<a href="javascript:;" id="forget-password">
						 here
					</a>
					 to reset your password.
				</p>
			</div>
		{!! Form::close() !!}

		<!-- END LOGIN FORM -->
		<!-- BEGIN FORGOT PASSWORD FORM -->
		{!! Form::open(['route' => 'sessions.email_reset_token', 'class' => 'forget-form']) !!}
			<h3>Forget Password ?</h3>
			<p>
				 {{ Alang::get('general.reset_your_password_below') }}
			</p>
			<div class="form-group">
				<div class="input-icon">
					<i class="fa fa-envelope"></i>
					<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> {{ Alang::get('general.back') }} </button>
				<button type="submit" class="btn blue pull-right">
				{{ Alang::get('general.submit') }} <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
		{!! Form::close() !!}
		<!-- END FORGOT PASSWORD FORM -->
	</div>
	<!-- END CONTAINER -->

@endsection


@push('styles')
	{!! Html::style(elixir('metronic/css/login.css')) !!}
@endpush

@push('scripts')
	{!! Html::script(elixir('metronic/js/login.js')) !!}
	<script>
		$(function() {     
			// Metronic.init(); // init metronic core components
			// Layout.init(); // init current layout
			Login.init();
			Demo.init();
			// init background slide images
			$.backstretch(
				[
					'{{ url('') }}/img/login/bg/1.jpg',
					'{{ url('') }}/img/login/bg/2.jpg',
					'{{ url('') }}/img/login/bg/3.jpg',
					'{{ url('') }}/img/login/bg/4.jpg'
				], {
					fade: 1000,
					duration: 8000
				}
			);
		});
	</script>
	<!-- END JAVASCRIPTS -->
@endpush