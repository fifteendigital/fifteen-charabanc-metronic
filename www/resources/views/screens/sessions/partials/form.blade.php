
<h5 class="form-section">{{ Alang::get('general.account_details') }}</h4>

<div class="form-group row @if ($errors->first('name')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.name') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('name', '<span class="error help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group row @if ($errors->first('url')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.url') }}:</label>
	<div class="col-md-9">
		{!! Form::text('url', null, ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('url', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<h5 class="form-section">{{ Alang::get('general.superuser_account') }}</h4>

<div class="form-group row @if ($errors->first('first_name')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.first_name') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('first_name', null, ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('first_name', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group row @if ($errors->first('surname')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.surname') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('surname', null, ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('surname', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group row @if ($errors->first('username')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.username') }}:<span class="required">*</span></label>
	<div class="col-md-9">
		{!! Form::text('username', null, ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('username', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group row @if ($errors->first('email')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.email') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('email', null, ['class' => 'form-control input-sm', 'autocomplete' => 'off']) !!}
		{!! $errors->first('email', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group row @if ($errors->first('password')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.password') }}:</label>
	<div class="col-md-9">
		{!! Form::password('password1', ['class' => 'form-control input-sm', 'autocomplete' => 'off']) !!}
		{!! $errors->first('password1', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group row @if ($errors->first('password')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.repeat_password') }}:</label>
	<div class="col-md-9">
		{!! Form::password('password2', ['class' => 'form-control input-sm', 'autocomplete' => 'off']) !!}
		{!! $errors->first('password2', '<span class="error help-block">:message</span>') !!}
	</div>
</div>
