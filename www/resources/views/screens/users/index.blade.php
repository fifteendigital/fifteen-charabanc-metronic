@extends('Template::layouts.master')

@section('page_title')

	<h3 class="page-title">{{ Alang::get('general.users') }}</h3>

@endsection

@section('content')

	<div class="portlet box blue-hoki">
		<div class="portlet-title">
			<div class="caption">
				{{ Alang::get('general.users') }}
			</div>
			<div class="actions">
				
				<a class="btn green" href="{{ route('users.create') }}">
					<i class="fa fa-plus"></i> {{ Alang::get('general.add_new') }}
				</a>
			</div>
		</div>
		<div class="portlet-body">
			
			@include('Template::partials.datatable_header')

			@if ($records->count())
				<div class="table-responsive">
					<table class="table dataTable table-striped table-hover table-bordered flip-content">
						<thead>
							<tr class="sort-header">
								{!! $data_table->sortBy('last_name', Alang::get('general.name')) !!}
								{!! $data_table->sortBy('email', Alang::get('general.email')) !!}
								<th>{{ Alang::get('general.roles') }}</th>
								<th class="text-center">{{ Alang::get('general.actions') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($records as $record)
								<tr>
									<td>{{ $record->first_name }} {{ $record->last_name }}</td>
									<td>{{ $record->email }}</td>
									<td>
										<?php $last_item = count($record->getRoles()) - 1; ?>
										@foreach ($record->getRoles() as $i => $role)
											{{ $role->name }}@if ($i != $last_item),@endif
										@endforeach
									</td>
									<td class="text-center">
										<a class="btn btn-xs purple" 
											href="{{ route('users.show', $record->id) }}" 
											title="{{ Alang::get('general.view_record') }}">
											<i class="fa fa-search"></i>
											{{ Alang::get('general.view') }}
										</a>
										&ensp;
										<a class="btn btn-xs blue" 
											href="{{ route('users.edit', $record->id) }}"
											title="{{ Alang::get('general.edit_record') }}">
											<i class="fa fa-pencil"></i>
											{{ Alang::get('general.edit') }}
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
				@include('Template::partials.datatable_footer')
				
				<div class="clearfix"></div>
			@else
				<p>{{ Alang::get('general.there_are_currently_no_records') }}. {!! link_to_route("users.create", Alang::get('general.create_a_new_record')) !!}.</p>
			@endif
			
		</div>
	</div>

@endsection
