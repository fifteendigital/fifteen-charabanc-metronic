@extends('Template::layouts.master')

@section('page_title')
	<h3 class="page-title">{{ Alang::get('general.new_user') }}</h3>
@endsection

@section('content')

	<div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i> {{ Alang::get('general.new_user') }}
			</div>
		</div>

		<div class="portlet-body form">

			{!! Form::open(['route' => 'users.store', 'role' => 'form', 'class' => 'form-horizontal']) !!}
			
				<div class="form-body">

					@include('screens.users.partials.form')
					
					<p>
						<em>* {{ Alang::get('general.required_fields') }}</em>
					</p>
				</div>

				<div class="form-actions">
					<div class="col-md-offset-3 col-md-9">
						<a class="btn btn-default" href="{{ route('users.index') }}">{{ Alang::get('general.back') }}</a>
						{!! Form::submit(Alang::get('general.save'), ['class' => 'btn btn-primary']) !!}
					</div>
				</div>

			{!! Form::close() !!}

		</div>

	</div>

@endsection