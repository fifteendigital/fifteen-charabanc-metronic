

<div class="form-group @if ($errors->first('first_name')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.first_name') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('first_name', null, ['class' => 'form-control']) !!}
		{!! $errors->first('first_name', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group @if ($errors->first('last_name')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.last_name') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('last_name', null, ['class' => 'form-control']) !!}
		{!! $errors->first('last_name', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group @if ($errors->first('email')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.email') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('email', null, ['class' => 'form-control']) !!}
		{!! $errors->first('email', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group @if ($errors->first('password')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.password') }}:</label>
	<div class="col-md-9">
		{!! Form::password('password1', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
		{!! $errors->first('password1', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group @if ($errors->first('password')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.repeat_password') }}:</label>
	<div class="col-md-9">
		{!! Form::password('password2', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
		{!! $errors->first('password2', '<span class="error help-block">:message</span>') !!}
	</div>
</div>

@if (Sentinel::getUser()->hasAccess('users.all'))
	<div class="form-group">
		<label class="col-md-3 control-label">{{ Alang::get('general.roles') }}:</label>
		<div class="col-md-9 checkbox-list">
			@foreach ($roles as $role)
				{!! Form::hidden('roles[' . $role->id . ']', 0) !!}
				<label>
					{!! Form::checkbox('roles[' . $role->id . ']',
						$role->id,
						( ! empty($record) && $record->inRole($role->id)) ? 1 : 0,
						['class' => 'form-control']) !!}
					{{ $role->name }}
				</label>
			@endforeach
		</div>
	</div>

	@if ( ! empty($record->id) && $record->last_login != '')
		<div class="form-group">
			<label class="col-md-3 control-label">{{ Alang::get('general.last_login') }}:</label>
			<div class="col-md-9">
				<p class="form-control-static">{{ $record->last_login->format('d/m/Y H:i') }}</p>
			</div>
		</div>
	@endif

@endif
