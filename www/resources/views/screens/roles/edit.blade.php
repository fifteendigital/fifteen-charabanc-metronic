@extends('Template::layouts.master')

@section('page_title')
	<h3 class="page-title">{{ $record->name }}</h3>
@endsection

@section('content')

	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i> {{ Alang::get('general.edit_record') }}
			</div>
			<div class="actions">
				<a href="#confirm_delete" class="red btn btn-warning" role="button" data-toggle="modal">
					<i class="fa fa-trash-o"></i> {{ Alang::get('general.delete') }}
				</a>
			</div>
		</div>

		<div class="portlet-body form">

			{!! Form::model($record, ['method' => 'PATCH', 'route' => ['roles.update', $record->id], 'role' => 'form', 'class' => 'form-horizontal']) !!}
				
				<div class="form-body">

					@include('screens.roles.partials.form')

					<p>
						<em>* {{ Alang::get('general.required_fields') }}</em>
					</p>
				</div>

				<div class="form-actions">
					<div class="col-md-offset-3 col-md-9">
						<a class="btn btn-default" href="{{ route('roles.show', $record->id) }}">
							{{ Alang::get('general.cancel') }}
						</a>
						{!! Form::submit(Alang::get('general.save'), ['class' => 'btn btn-primary']) !!}
					</div>
				</div>

			{!! Form::close() !!}

		</div>
		
	</div>

	@include('screens.roles.partials.delete')

@endsection