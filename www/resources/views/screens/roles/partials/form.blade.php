
<div class="form-group @if ($errors->first('name')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.name') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('name', null, ['class' => 'form-control']) !!}
		{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-group @if ($errors->first('slug')) has-error @endif">
	<label class="col-md-3 control-label">{{ Alang::get('general.slug') }}<span class="required">*</span>:</label>
	<div class="col-md-9">
		{!! Form::text('slug', null, ['class' => 'form-control']) !!}
		{!! $errors->first('slug', '<span class="help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group">
	<label class="col-md-3 control-label">{{ Alang::get('general.permissions') }}:</label>
	<div class="col-md-9 checkbox-list">
		@include('screens.roles.partials.edit_permissions')
	</div>
</div>
