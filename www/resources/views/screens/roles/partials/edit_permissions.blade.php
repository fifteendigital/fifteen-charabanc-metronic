
{!! Form::hidden('permissions[menu.admin]', 0) !!}
<label>
	{!! Form::checkbox('permissions[menu.admin]', 
		1, 
		( ! empty($record) && $record->hasAccess('menu.admin')) ? 1 : 0, 
		['class' => 'form-control']) !!}
	{{ Alang::get('general.access_to_admin_menu') }}
</label>

{!! Form::hidden('permissions[roles.all]', 0) !!}
<label>
	{!! Form::checkbox('permissions[roles.all]', 
		1, 
		( ! empty($record) && $record->hasAccess('roles.all')) ? 1 : 0, 
		['class' => 'form-control']) !!}
	{{ Alang::get('general.manage_user_roles') }}
</label>

{!! Form::hidden('permissions[users.all]', 0) !!}
<label>
	{!! Form::checkbox('permissions[users.all]', 
		1, 
		( ! empty($record) && $record->hasAccess('users.all')) ? 1 : 0, 
		['class' => 'form-control']) !!}
	{{ Alang::get('general.manage_user_accounts') }}
</label>
