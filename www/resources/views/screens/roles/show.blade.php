@extends('Template::layouts.master')

@section('page_title')
	<h3 class="page-title">{{ $record->name }}</h3>
@endsection

@section('content')

	<div class="portlet box purple">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-reorder"></i> {{ Alang::get('general.view_record') }}
			</div>
			<div class="actions">
				<a class="btn blue" href="{{ route('roles.edit', $record->id) }}">
					<i class="fa fa-pencil"></i> {{ Alang::get('general.edit') }}
				</a>
			</div>
		</div>

		<div class="portlet-body form">

			<form class="form-horizontal">
				
				<div class="form-body">

					@include('screens.roles.partials.data')

				</div>

				<div class="form-actions">
					<div class="col-md-offset-3 col-md-9">
						<a class="btn btn-default" href="{{ route('roles.index') }}">
							{{ Alang::get('general.back') }}
						</a>
					</div>
				</div>

			</form>

		</div>
		
	</div>

@endsection