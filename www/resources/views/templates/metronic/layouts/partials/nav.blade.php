<li class="
	@if ($item->isActive()) active @endif
	@if ($i == 0 and $item->level == 0) start @endif
	">
	@if ($item->hasChildren())
		<a href="javascript:;">
			@if ($item->icon)
				<i class="fa {{ $item->icon }}"></i>
			@endif
			<span class="title">
				{{ $item->title }}
			</span>
			@if ($item->isActive())
				<span class="selected"></span>
				<span class="arrow open"></span>
			@else
				<span class="arrow"></span>
			@endif
			
		</a>
		<ul class="sub-menu">
			@foreach ($item->children as $i => $item)
				@include('Template::layouts.partials.nav')
			@endforeach
		</ul>
	@else
		<a href="{{ $item->getUrl() }}">
			@if ($item->icon)
				<i class="fa {{ $item->icon }}"></i>
			@endif
			<span class="title">
				{{ $item->title }}
			</span>
			@if ($item->isActive())
				<span class="selected"></span>
			@endif
		</a>
	@endif
</li>