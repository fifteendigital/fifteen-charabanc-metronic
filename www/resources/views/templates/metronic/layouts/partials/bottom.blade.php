<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
         &copy; {{ date('Y') }} Fifteen Digital
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
