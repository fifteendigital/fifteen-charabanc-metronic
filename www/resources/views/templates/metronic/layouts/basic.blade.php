<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!-- BEGIN HEAD -->
	<head>
		{!! MetaTags::render() !!}
		@include('Template::layouts.partials.header')
		@yield('header_custom')
		@stack('styles')
	</head>
	<!-- END HEAD -->
	<!-- BEGIN BODY -->
	<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
	<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
	<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
	<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
	<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
	<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
	<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
	<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
	<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
	<body class="@yield('body_classes', 'page-header-fixed page-style-square page-sidebar-closed page-sidebar-hide')">

		<!-- BEGIN HEADER -->
		@include('Template::layouts.partials.top-basic')
		<!-- END HEADER -->

		<div class="clearfix">
		</div>
		<!-- BEGIN CONTAINER -->
		<div class="page-container">

			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">

					@yield('content')
					
				</div>	
			</div>
			<!-- END CONTENT -->
		</div>
		<!-- END CONTAINER -->

		<!-- BEGIN FOOTER -->
		@include('Template::layouts.partials.bottom')
		<!-- END FOOTER -->

		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		@include('Template::layouts.partials.footer')

		<!-- END JAVASCRIPTS -->
		@stack('scripts')
		
	</body>
	<!-- END BODY -->
	
</html>