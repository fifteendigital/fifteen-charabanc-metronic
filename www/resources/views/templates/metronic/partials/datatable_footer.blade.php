
@if ($records->lastPage() > 1)
    <div class="row data_table_controls">
        <div class="col-md-5 col-sm-12">
            <div class="dataTables_info" id="sample_2_info">
                {{ Alang::get('general.showing') }} {{ $records->firstItem() }}
                    {{ strtolower(Alang::get('general.to')) }} {{ $records->lastItem() }}
                    {{ strtolower(Alang::get('general.of')) }}  {{ $records->total() }}
                    {{ strtolower(Alang::get('general.entries')) }}
            </div>
        </div>
        <div class="col-md-7 col-sm-12">
            <div class="dataTables_paginate paging-bootstrap hidden-print">
                {!! $records->links('Template::partials.datatable_links') !!}
            </div>
        </div>
    </div>
@endif
