
        <div class="form-group">
            <label class="col-md-3 control-label">{{ lang('$LANG_APP$.$LABEL$') }}:</label>
            <div class="col-md-9">
                <p class="form-control-static">
                    @if (!empty($record->$NAME$))
                        <a href="{{ url('/images/' . $record->$NAME$) }}" target="_blank">
                            <img src="{{ url('/thumb/fit/400x100/' . $record->$NAME$) }}" alt="{{ basename($record->$NAME$) }}" />
                        </a>
                    @endif
                </p>
            </div>
        </div>
