
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{ lang('$LANG_APP$.$LABEL$') }}$REQUIRED_STUB$:</label>
                            <div class="col-md-9">
                                @if (!empty($record->$NAME$))
                                    {!! form_edit_file('$NAME$', $record->$NAME$) !!}
                                @else
                                    {!! Form::file('$NAME$') !!}
                                @endif
                            </div>
                        </div>
